// 1. Попросіть користувача ввести свій вік за допомогою prompt.
// Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те,
// що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те,
// що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.

const requestAge = prompt("Введіть ваш вік:");

if (requestAge <= 12) {
  alert("Ти дитина!");
} else if (requestAge <= 18) {
  alert("Ти вже підліток");
} else {
  alert("Ти дорослий!");
}

// 1. Перевірте чи ввів користувач саме число в поле prompt. Якщо ні, то виведіть повідомлення, що введено не число.
// 2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення,
// скільки днів у цьому місяці. Результат виводиться в консоль.

const month = prompt("Який місяць?").toLowerCase();

if (typeof month == "string") {
  switch (month) {
    case "січень":
      console.log(`В ${month} маємо 31 день`);
      break;
    case "лютий":
      console.log(`В ${month} маємо 28 день`);
      break;
    case "березень":
      console.log(`В ${month} маємо 31 день`);
      break;
    case "квітень":
      console.log(`В ${month} маємо 30 день`);
      break;
    case "травень":
      console.log(`В ${month} маємо 31 день`);
      break;
    case "червень":
      console.log(`В ${month} маємо 30 день`);
      break;
    case "липень":
      console.log(`В ${month} маємо 31 день`);
      break;
    case "серпень":
      console.log(`В ${month} маємо 31 день`);
      break;
    case "вересень":
      console.log(`В ${month} маємо 30 день`);
      break;
    case "жовтень":
      console.log(`В ${month} маємо 31 день`);
      break;
    case "листопад":
      console.log(`В ${month} маємо 30 день`);
      break;
    case "грудень":
      console.log(`В ${month} маємо 31 день`);
      break;
  }
} else {
  console.log(`${month} не є жодним місяцем`);
}
